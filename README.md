# Test of automated a11y checkers
<em> [Pa11y](https://github.com/pa11y/) and [aXe](https://github.com/dequelabs/axe-core).</em>

#Scope
This project tests coverage and correctness of the aXe and pa11y accessibility
checkers for WCAG 2.0 level AA. Rules that produces notices or warnings (i.e not conformable by automated checking only)
are excluded from the test. 

#Running tests
##Start server
<em>NB! Both tests requires a running server instance</em><br/> 
```npm run dev```<br/>
##Run axe
```npm run axe ```<br/>
Results found in [axe results directory](checkers/axe/results)
##Run pa11y
```npm run pa11y``` <br/>
Results found in [pa11y results directory](checkers/axe/results)






