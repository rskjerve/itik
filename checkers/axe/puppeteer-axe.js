/**
axe-core with puppeter
**/

const puppeteer = require('puppeteer');
const axeCore = require('axe-core');
const _ = require('lodash')
const fs = require('fs')
var utils = module.exports = {}


utils.main = async url => {
	let browser;
	let results;
	try {
		// Setup Puppeteer
		browser = await puppeteer.launch();

		// Get new page
		const page = await browser.newPage();
		await page.goto(url);


		// Inject and run axe-core
		const handle = await page.evaluateHandle(`
			// Inject axe source code
			${axeCore.source}

			// Run axe
			axe.run()
		`);

		// Get the results from `axe.run()`.
		results = await handle.jsonValue();
		// Destroy the handle & return axe results.
		await handle.dispose();
	} catch (err) {
		// Ensure we close the puppeteer connection when possible
		if (browser) {
			await browser.close();
		}

		// Re-throw
		throw err;
	}

	await browser.close();
	return results;
};

utils.sortResults = function(result, outputObject,date){
   let rawdata = {
   	axe_results: result,
   	w3c_errors: outputObject.w3c.erros,
   	w3c_tags: outputObject.tags
   }

    const dir_name = "./checkers/axe/results/"+date;

    fs.writeFile(dir_name+"/"+date+"_"+outputObject.page+".json", JSON.stringify(rawdata, null, 4), (err) => {
        if (err) {
            console.error(err);
            return;
        };
        console.log("File has been created");
    });

   outputObject.axe={}
   outputObject.axe.erros = result.violations.length;

    const wcag_tags =[]
        _.each(result.violations,function (o) {
        	_.each(o.tags, function(tag){
        		if (tag.match(/^wcag[0-9][0-9]/)){
        			wcag_tags.push(tag);
				}
			});
        });
    outputObject.axe.tags = wcag_tags;
    outputObject.falsePositives = _.difference(wcag_tags, outputObject.w3c.tags);
    outputObject.falseNegatives = _.difference(outputObject.w3c.tags,wcag_tags);
    outputObject.truePositives = _.intersection(outputObject.w3c.tags,wcag_tags);
    outputObject.str= outputObject.page + ":\n"+ outputObject.truePositives.length + "/"+ outputObject.w3c.tags.length +" errors found \nNumber of false positives:" + outputObject.falsePositives.length
	+ "\nfailure instances:" + wcag_tags.length+
      "\nfailure tags: " + _.uniq(wcag_tags)+
      "\ntrue positives:" + outputObject.truePositives.length+
      "\nfalse positives:" + outputObject.falsePositives.length+
      "\nfalse negatives: " + outputObject.falseNegatives.length+'\n';

	return outputObject
}
