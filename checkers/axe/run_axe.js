const utils = require('./puppeteer-axe')
const w3c = require('../w3cDefinedErrors.js')
const fs = require('fs');
date = require('date-fns')
const d = date.format(new Date(), 'DD-MM-YYYY-hh-mm-ss')
const dir_name = "./checkers/axe/results/"+d;
fs.mkdir(dir_name, { recursive: true }, (err) => {
    if (err) throw err;
});
const logStream = fs.createWriteStream(dir_name+'/'+d+'_Overview.txt', {'flags': 'a'});
const dPretty = date.format(new Date(),'DD-MM-YYYY')
logStream.write("TEST RESULTS OVERVIEW\n" + dPretty+'\nTesting on conformance level AA\n');

utils.main("http://localhost:3000/src/before/home.html")
    .then(results => {
        let output = {};
        output.page = "Home";
        output.w3c = {
              errors : w3c.axe.home.length,
              tags: w3c.axe.home
        }
        const resultOutput = utils.sortResults(results,output,d);
        logStream.write(resultOutput.str);
    })
    .catch(err => {
        console.error('Error running axe-core:', err.message);
        process.exit(1);
    });

utils.main("http://localhost:3000/src/before/news.html")
    .then(results => {
        let output = {};
        output.page = "News"
        output.w3c = {
            errors : w3c.axe.news.length,
            tags: w3c.axe.news
        }
        const resultOutput = utils.sortResults(results,output,d);
        logStream.write(resultOutput.str);
    })
    .catch(err => {
        console.error('Error running axe-core:', err.message);
        process.exit(1);

    });


utils.main("http://localhost:3000/src/before/tickets.html")
    .then(results => {
        let output = {};
        output.page = "Tickets"
        output.w3c = {
            errors : w3c.axe.tickets.length,
            tags: w3c.axe.tickets
        }
        const resultOutput = utils.sortResults(results,output,d);
        logStream.write(resultOutput.str);
    })
    .catch(err => {
        console.error('Error running axe-core:', err.message);
        process.exit(1);
    });

utils.main("http://localhost:3000/src/before/survey.html")
    .then(results => {
        let output = {};
        output.page = "Survey"
        output.w3c = {
            errors : w3c.axe.survey.length,
            tags: w3c.axe.survey
        }
        const resultOutput = utils.sortResults(results,output,d);
        logStream.write(resultOutput.str);

    })
    .catch(err => {
        console.error('Error running axe-core:', err.message);
        process.exit(1);
    });

