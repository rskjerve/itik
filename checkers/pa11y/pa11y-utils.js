'use strict'

var utils = module.exports = {}
const _ = require('lodash')
const fs =require('fs')

utils.sortResults = function(result, outputObject,date){
    const rawdata = {
        pa11y_results: result,
        w3c_errors: outputObject.w3c.erros,
        w3c_tags: outputObject.tags
    }
    outputObject.pa11y_results = result;

    const dir_name = "./checkers/pa11y/results/"+date;
     fs.writeFile(dir_name+"/"+date+"_"+_.capitalize(outputObject.page)+".json", JSON.stringify(rawdata, null, 4), (err) => {
        if (err) {
            console.error(err);
            return;
        };
    });

    outputObject.pa11y=[];
    let pa11y_tags = []
    _.each(result.issues, function (o) {
        pa11y_tags.push(o.code);
    })
    const prepTags = utils.prepareTags(pa11y_tags);
    outputObject.falsePositives = _.difference(prepTags, outputObject.w3c.tags);
    outputObject.falseNegatives = _.difference(outputObject.w3c.tags,prepTags);
    outputObject.truePositives =_.intersection(outputObject.w3c.tags,prepTags)


    outputObject.str= '\n'+_.capitalize(outputObject.page) + ":\n"+outputObject.truePositives.length + "/"+ outputObject.w3c.tags.length +" guideline failures found" +
        "\nfailure instances:" + pa11y_tags.length+
        "\nfailure tags: " + _.uniq(prepTags)+
        "\ntrue positives:" + outputObject.truePositives.length+
        "\nfalse positives:" + outputObject.falsePositives.length+
        "\nfalse negatives: " + outputObject.falseNegatives.length+'\n';


    /*"\nNumber of false positives:" + outputObject.falsePositives.length
    + "\nNot detected: " + outputObject.falseNegatives+"\n\n";*/
    return outputObject
}

utils.prepareTags = function (tags) {
    const prep=[];
    _.each(tags, function(tag){
        const split = tag.split('.');
        prep.push(split[0]+'.'+split[1]+'.'+split[2]+'.'+split[3])
    });

        return prep;
    }