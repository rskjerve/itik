'use strict'
const pa11y = require('pa11y')
const w3c = require('../w3cDefinedErrors.js')
const fs = require('fs');
const date = require('date-fns')
const d = date.format(new Date(), 'DD-MM-YYYY-hh-mm-ss')
const dir_name = "./checkers/pa11y/results/"+d;
fs.mkdir(dir_name, { recursive: true }, (err) => {
    if (err) throw err;
});

const logStream = fs.createWriteStream(dir_name+'/'+d+'_Overview.txt', {'flags': 'a'});
const dPretty = date.format(new Date(),'DD-MM-YYYY')
const utils = require('./pa11y-utils')

logStream.write("TEST RESULTS OVERVIEW (pa11y)\n" + dPretty+'\nTesting on conformance level AA\n');

runAudit()

function runAudit(){
    auditPage("http://localhost:3000/src/before/home.html","home");
    auditPage("http://localhost:3000/src/before/news.html","news");
    auditPage("http://localhost:3000/src/before/tickets.html","tickets");
    auditPage("http://localhost:3000/src/before/survey.html","survey");
}

function auditPage(test_url,feature) {
    let output = {};
    output.page = feature;
    output.w3c = {
        errors: w3c.pa11y[feature].length,
        tags:w3c.pa11y[feature]
    }
    try {
        pa11y(test_url).then((results) => {
            output = utils.sortResults(results,output,d)
            logStream.write(output.str)
    })} catch(error){
        console.log(error.message)
    }
}
